package org.andresilvaalves.reduce;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.log4j.Logger;

public class AverageReduce extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {

	private static final Logger LOG = Logger.getLogger(AverageReduce.class);

	
	@Override
	protected void reduce(Text year, Iterable<DoubleWritable> temperatures,
			Reducer<Text, DoubleWritable, Text, DoubleWritable>.Context context)
			throws IOException, InterruptedException {

		LOG.info("Executing Average Reduce");
		LOG.info("Year = "+year.toString());
		
		double sum = 0;
		int count = 0;
		for (DoubleWritable temperature : temperatures) {
			sum += temperature.get();
			count++;
		}

		double average = sum / count;
		context.write(year, new DoubleWritable(average));
		LOG.info("Average = "+average);
		
	}
}
